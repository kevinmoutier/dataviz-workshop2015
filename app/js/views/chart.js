import d3 from 'd3';

export default class Chart {
	constructor(selector) {

		this.selector = selector;
		// Append a group into the svg.charts div
		this.svg = d3.select("." + selector)
			.append("g")

		// Append donuts slice
		this.svg.append("g")
			.attr("class", "slices");

		// Append group for labels
		this.svg.append("g")
			.attr("class", "labels");

		// Append group for lines
		this.svg.append("g")
			.attr("class", "lines");

		var area = document.getElementById(selector).getBoundingClientRect();

		// Width and Height of the Chart Div
		var width = area.width,
		    height = area.height;
			
		this.radius = Math.min(width, height) / 2;

		// Create a pie D3 layout : A layout include a strategy of a specific chart. Here the Pie
		this.pie = d3.layout.pie()
			.sort(null)
			.value(function(d) {
				return d.value;
			});

		// The inner arc for the hole
		this.arc = d3.svg.arc()
			.outerRadius(this.radius * 0.8)
			.innerRadius(this.radius * 0.6);

		// The outer arc
		this.outerArc = d3.svg.arc()
			.innerRadius(this.radius * 0.9)
			.outerRadius(this.radius * 0.9);

		// Position of the Chart : in the center of the selected SVG
		this.svg.attr("transform", "translate(" + width / 1.5 + "," + height / 2 + ")");


		// Create a function in key to return the label
		this.key = function(d){ 
			return d.data.label;
		};

		// Associate Colors
		this.color = d3.scale.ordinal()
			.domain([0,100])
			.range(["#6dcad6", "#075889", "#f23012", "#640c00", "#840c00"]);
	}

	// Fonction to Randomize Date for test
	randomData (){
		var labels = this.color.domain();
		return labels.map(function(label){
			return { label: label, value: Math.random() }
		});
	}

	/**
	 * init 
	 * init the charts
	 * @param  {[array]} The Data to put in the chart
	 */
	init(data) {

		console.log(" ****** Initialisation du chart "+this.selector+" 	******");
		/* ------- PIE SLICES -------*/
		var that = this;
		var slice = this.svg.select(".slices").selectAll("path.slice")
			.data(this.pie(data), this.key);
		// Create Slice
		slice.enter() 
			.insert("path") // Insert a path 
			.style("fill", (d) => { return this.color(d.data.label); }) // Fill with the color function : one color match with one label
			.attr("class", "slice") // Add class
			.attr("d", (d, i)  => {
				this._current = this._current || d; // Animation in current position or d value
				var interpolate = d3.interpolate(this._current, d); // Interpolate from last value to d value
				this._current = interpolate(0);
				return this.arc(d);
			});

		// Anime Slice Transition
		 slice		
			.transition().duration(1000) // Animation 1 seconde
			.attrTween("d", (d) => {
				this._current = this._current || d; // Animation in current position or d value
				var interpolate = d3.interpolate(this._current, d); // Interpolate from last value to d value
				this._current = interpolate(0);
				return (t) => {
					return this.arc(interpolate(t));
				};
			})

		slice.exit()
			.remove();

		/* ------- TEXT LABELS -------*/

		// Prepare labels
		var text = this.svg.select(".labels").selectAll("text")
			.data(this.pie(data), this.key);

		// Append text
		text.enter()
			.append("text")
			.attr("dy", ".35em")
			.text(function(d) {
				return d.data.value + "% de " + d.data.label;
			});
		
		function midAngle(d){
			return d.startAngle + (d.endAngle - d.startAngle)/2;
		}

		text.transition().duration(1000)
			.attrTween("transform", (d) => {
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return (t) => {
					var d2 = interpolate(t);
					var pos = this.outerArc.centroid(d2);
					pos[0] = this.radius * (midAngle(d2) < Math.PI ? 1 : -1);
					return "translate("+ pos +")";
				};
			})
			.styleTween("text-anchor", function(d){
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return function(t) {
					var d2 = interpolate(t);
					return midAngle(d2) < Math.PI ? "start":"end";
				};
			}).style("opacity", function(d){
		  		return "0.7";
			});

		// Exit text
		text.exit()
			.remove();

		/* ------- SLICE TO TEXT POLYLINES -------*/

		// Prepare Lines
		var polyline = this.svg.select(".lines").selectAll("polyline")
			.data(this.pie(data), this.key);
		
		// Apppend Lines
		polyline.enter()
			.append("polyline");

		// Lines transition
		polyline.transition().duration(1000)
			.attrTween("points", (d) => {
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return (t) => {
					var d2 = interpolate(t);
					var pos = this.outerArc.centroid(d2);
					pos[0] = this.radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
					return [this.arc.centroid(d2), this.outerArc.centroid(d2), pos];
				};			
			})
			.style("opacity", function(d){
		  		return "0.7";
			});

		// Exit Polyline
		polyline.exit()
			.remove();
	}

	/**
	 * change 
	 * Change the charts
	 * @param  {[array]} The Data to put in the chart
	 */
	change(data) {
		/* ------- PIE SLICES -------*/
		var that = this;
		var slice = this.svg.select(".slices").selectAll("path.slice")
			.data(this.pie(data), this.key);

		// Create Slice
		slice.enter() 
			.insert("path") // Insert a path 
			.style("fill", (d) => { return this.color(d.data.label); }) // Fill with the color function : one color match with one label
			.attr("class", "slice") // Add class
			.attr("d", function (d, i) {
				this._current = this._current || d; // Animation in current position or d value
				var interpolate = d3.interpolate(this._current, d); // Interpolate from last value to d value
				this._current = interpolate(0);
				return that.arc(d);
			});

		// Anime Slice Transition
		 slice		
			.transition().duration(1000) // Animation 1 seconde
			.attrTween("d", function(d) {
				this._current = this._current || d; // Animation in current position or d value
				var interpolate = d3.interpolate(this._current, d); // Interpolate from last value to d value
				this._current = interpolate(0);
				return (t) => {
					return that.arc(interpolate(t));
				};
			})

		slice.exit()
			.remove();

		/* ------- TEXT LABELS -------*/

		// Prepare labels
		var text = this.svg.select(".labels").selectAll("text")
			.data(this.pie(data), this.key);

		// Append text
		text.enter()
			.append("text");

		//text.exit().remove();

		text
			.attr("dy", ".35em")
			.text( (d) => {
				return d.data.value + "% de " + d.data.label;
			});
		
		function midAngle(d){
			return d.startAngle + (d.endAngle - d.startAngle)/2;
		}

		text.transition().duration(1000)
			.attrTween("transform", function (d) {
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return (t) => {
					var d2 = interpolate(t);
					var pos = that.outerArc.centroid(d2);
					pos[0] = that.radius * (midAngle(d2) < Math.PI ? 1 : -1);
					return "translate("+ pos +")";
				};
			})
			.styleTween("text-anchor", function(d){
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return function(t) {
					var d2 = interpolate(t);
					return midAngle(d2) < Math.PI ? "start":"end";
				};
			});

		// Exit text
		text.exit()
			.remove();

		/* ------- SLICE TO TEXT POLYLINES -------*/

		// Prepare Lines
		var polyline = this.svg.select(".lines").selectAll("polyline")
			.data(this.pie(data), this.key);
		
		// Apppend Lines
		polyline.enter()
			.append("polyline")
			.attr("points", function (d) {
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
			})
			;

		// Lines transition
		polyline.transition().duration(1000)
			.attrTween("points", function (d) {
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return (t) =>  {
					var d2 = interpolate(t);
					var pos = that.outerArc.centroid(d2);
					pos[0] = that.radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
					return [that.arc.centroid(d2), that.outerArc.centroid(d2), pos];
				};			
			});

		// Exit Polyline
		polyline.exit()
			.remove();
	}

	/**
	 * terminate 
	 * Close chart
	 */
	terminate() {

		var data = [
			{ label: "concert", value: 0 },
			{ label: "atelier", value: 0 },
			{ label: "cinema", value: 0 },
			{ label: "theatre", value: 0 },
			{ label: "conference", value: 0 }
		];

		console.log(" ****** Fermeture du chart "+this.selector+" ******");
		/* ------- PIE SLICES -------*/
		var that = this;
		var slice = this.svg.select(".slices").selectAll("path.slice")
			.data(this.pie(data), this.key);
		// Create Slice
		slice.enter() 
			.insert("path") // Insert a path 
			.attr("class", "slice") // Add class
			.attr("d", (d, i)  => {
				this._current = this._current || d; // Animation in current position or d value
				var interpolate = d3.interpolate(this._current, d); // Interpolate from last value to d value
				this._current = interpolate(0);
				return this.arc(d);
			});

		// Anime Slice Transition
		 slice		
			.transition().duration(1000) // Animation 1 seconde
			.attrTween("d", (d) => {
				// console.log(this, that);
				this._current = this._current || d; // Animation in current position or d value
				var interpolate = d3.interpolate(this._current, d); // Interpolate from last value to d value
				this._current = interpolate(0);
				return (t) => {
					return this.arc(interpolate(t));
				};
			})

		slice.exit()
			.remove();

		/* ------- TEXT LABELS -------*/

		// Prepare labels
		var text = this.svg.select(".labels").selectAll("text")
			.data(this.pie(data), this.key);

		// Append text
		text.enter()
			.append("text")
			.attr("dy", ".35em")
			.text(function(d) {
				return d.data.value + "% de " + d.data.label;
			});
		
		function midAngle(d){
			return d.startAngle + (d.endAngle - d.startAngle)/2;
		}

		text.transition().duration(1000)
			.attrTween("transform", (d) => {
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return (t) => {
					var d2 = interpolate(t);
					var pos = this.outerArc.centroid(d2);
					pos[0] = this.radius * (midAngle(d2) < Math.PI ? 1 : -1);
					return "translate("+ pos +")";
				};
			})
			.styleTween("text-anchor", function(d){
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return function(t) {
					var d2 = interpolate(t);
					return midAngle(d2) < Math.PI ? "start":"end";
				};
			})
			.style("opacity", function(d){
		  		return "0";
			});

		// Exit text
		text.exit()
			.remove();

		/* ------- SLICE TO TEXT POLYLINES -------*/

		// Prepare Lines
		var polyline = this.svg.select(".lines").selectAll("polyline")
			.data(this.pie(data), this.key);
		
		// Apppend Lines
		polyline.enter()
			.append("polyline");

		// Lines transition
		polyline.transition().duration(1000)
			.attrTween("points", (d) => {
				this._current = this._current || d;
				var interpolate = d3.interpolate(this._current, d);
				this._current = interpolate(0);
				return (t) => {
					var d2 = interpolate(t);
					var pos = this.outerArc.centroid(d2);
					pos[0] = this.radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
					return [this.arc.centroid(d2), this.outerArc.centroid(d2), pos];
				};			
			})
			.style("opacity", function(d){
		  		return "0";
			});

		// Exit Polyline
		polyline.exit()
			.remove();
	}



	update() {

	}
}