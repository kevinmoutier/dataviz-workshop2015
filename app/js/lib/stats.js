import ArrayUtils from "../utils/array-utils"

export default class Stats {
	/**
	* @constructor
	*/
	constructor() {

		this.districts = ["Louvre", "Bourse", "Temple", "Hôtel de ville", "Panthéon", "Luxembourg", "Palais-Bourbon", "Elysée", "Opéra", "Entrepôt", "Popincourt", "Reuilly", "Gobelins", "L'Observatoire", "Vaugirard", "Trocadero", "Batignolles-Monceaux", "Butte-Montmartre", "Buttes-Chaumont", "Ménilmontant" ];

	}

	/**
	 * orderBy 
	 * - Give the number of Event in one or multiple categories for each district
	 * @param  {json} data [The json data to load for all events]
	 * @param  {Array} args [ The category ]
	 * @return {[Array]} classment [ An array with all result data ]
	 */
	orderBy(data, args) {
		var events = ArrayUtils.jsonToArray(data);
		
		var classment = [];
		var nbEvent = 0;

		for (let a = 1 ; a <= 20; a++) {
			for (let n in args) {
				for (let i=0; i < events.length; i++) {
					if (events[i].category == args[n] && events[i].arrondissement == a ) {
						 nbEvent++;
					}
				}
			}

			classment.push({
				arrondissement: a,
				nb: nbEvent
			});

			nbEvent = 0;
		}

		return classment;
	}

	/**
	 * allStats 
	 * - Get all statistic of District
	 * @param  {[json]} data           [The json main data]
	 * @param  {[int]} arrondissement [ The choosen districtv  ]
	 * @return {[Obj]}                [ A object with statistic of the districtn total and percents]
	 */
	allStats (data,arrondissement) {
		var events = ArrayUtils.jsonToArray(data);

		var total = 0;
		var concert = 0;
		var atelier = 0;
		var cinema = 0;
		var conference = 0;
		var theatre = 0;

		for (let i=0; i < events.length; i++) {

			if (events[i].arrondissement == arrondissement) {
				
				if(events[i].category == "Concert") {
						concert ++;
						total++;
				}
				if(events[i].category == "Atelier") {
						atelier ++;
						total++;
				}
				if(events[i].category == "Cinema" || events[i].category == "Photographie" ) {
						cinema ++;
						total++;
				}
				if(events[i].category == "Conference" || events[i].category == "Event" || events[i].category == "<Salon></Salon>"  ) {
						conference ++;
						total++;
				}
				if(events[i].category == "Theatre") {
						theatre ++;
						total++;
				}
			}

		}

		var result = [
			{ label: "concert", value: parseFloat(((concert/total)*100).toFixed(2)) },
			{ label: "atelier", value: parseFloat(((atelier/total)*100).toFixed(2)) },
			{ label: "cinema", value: parseFloat(((cinema/total)*100).toFixed(2)) },
			{ label: "theatre", value: parseFloat(((theatre/total)*100).toFixed(2)) },
			{ label: "conference", value: parseFloat(((conference/total)*100).toFixed(2)) }
		];

		return result;
	}

	/**
	 * getMax
	 * -  Get the district which have the more events of given categories
	 * @param  {json} data [The json data to load for all events]
	 * @param  {Array} args [ The category ]
	 * @return {[Array]}      [ An array with district and it's total event number ]
	 */
	getMax(data, args) {

		var result = [];

		var numbers = this.orderBy(data, args);
		var theOne = numbers[0]["arrondissement"];
		var max = numbers[0]["nb"];
		var index = 0;

		for (let i = 1; i < 19; i++) {
			if (numbers[(i+1)]["nb"] > max) {
				index = i+1;
				theOne = numbers[(i+1)]["arrondissement"];
				max = numbers[i+1]["nb"];;
			}
		}

		numbers.splice(index, 1);

		result["One"] = {
			"arr" : theOne,
			"nb" : max,
			"district" : this.districts[(theOne-1)]
		};

		var theTwo= numbers[0]["arrondissement"];
		max = numbers[0]["nb"];
		var index = 0;

		for (let i = 1; i < 18; i++) {
			if (numbers[(i+1)]["nb"] > max) {
				index = i+1;
				theTwo = numbers[(i+1)]["arrondissement"];
				max = numbers[i+1]["nb"];;
			}
		}

		result["Two"] = {
			"arr" : theTwo,
			"nb" : max,
			"district" : this.districts[(theTwo-1)]
		};


		return result;
	}
}