import EventEmitter from '../lib/event-emitter';

export default class Data {

	/**
	* @constructor
	*/
	constructor() {
		this.data = [];

	}

	/**
	 * update
	 * - Update values
	 */
	update() {
	}

	getData(callback) {
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
				if(xmlhttp.status == 200){
					// Callback qui renvoie les données en asynchrone
					callback(xmlhttp.responseText);
			   }
			   else if(xmlhttp.status == 400) {
					alert('There was an error 400');
				}
				else {
					alert('something else other than 200 was returned');
				}
			}
		}
		xmlhttp.open("GET", "data/data.json", true);
		xmlhttp.send();
	}
}