import Dat from 'dat-gui';
import EventEmitter from './lib/event-emitter';
import Data from './data/data';
import Stats from './lib/stats';
import Chart from './views/chart';
import NumbersUtils from './utils/number-utils';
// import $ from 'jquery';
	

class App {

	/**
	* @constructor
	*/
	constructor() {

		// The activated type of Events
		this.activated = [];

		this.DELTA_TIME = 0;
		this.LAST_TIME = Date.now();

		this.width = window.innerWidth;
		this.height = window.innerHeight;

		this.data = new Data();
		this.primeChart = new Chart("prime-chart");
		this.secondChart = new Chart("second-chart");
		this.data.getData( (res) => {
			this.json = res;
		});

		this.stat = new Stats();

		this.addListeners();


		HTMLElement.prototype.hasClass = function (className) {
		    if (this.classList) {
		        return this.classList.contains(className);
		    } else {
		        return (-1 < this.className.indexOf(className));
		    }
		};

		HTMLElement.prototype.addClass = function (className) {
		    if (this.classList) {
		        this.classList.add(className);
		    } else if (!this.hasClass(className)) {
		        var classes = this.className.split(" ");
		        classes.push(className);
		        this.className = classes.join(" ");
		    }
		    return this;
		};

		HTMLElement.prototype.removeClass = function (className) {
		    if (this.classList) {
		        this.classList.remove(className);
		    } else {
		        var classes = this.className.split(" ");
		        classes.splice(classes.indexOf(className), 1);
		        this.className = classes.join(" ");
		    }
		    return this;
		};
	}

	/**
	 * addListeners
	 */
	addListeners() {
		window.addEventListener( 'resize', this.onResize.bind(this) );

		// Toogle when click on type of event
		var cell = document.querySelectorAll('.event');
		for(var i=0;i<cell.length;i++){
			cell[i].addEventListener('click',this.toogle.bind(this), false);
		}

		TweenMax.ticker.addEventListener( 'tick', this.update.bind(this))
	}


	/**
	 * update
	 * - Triggered on every TweenMax tick
	 */
	update() {
		this.DELTA_TIME = Date.now() - this.LAST_TIME;
		this.LAST_TIME = Date.now();
	}

	toogle(el) {

		var max, statisticOne, statisticTwo;
		var nbActive = this.activated.length;

		if(!el.target.classList.contains("active")) {
			// If the category is disable, enable it !

			this.activated.push(el.target.getAttribute('data-type'));

			max = this.stat.getMax(this.json, this.activated);
			statisticOne = this.stat.allStats(this.json, max["One"]["arr"]);
			statisticTwo = this.stat.allStats(this.json, max["Two"]["arr"]);
			
			if(nbActive === 0) {
			// If any Category is selected

				this.primeChart.init(statisticOne); // Initialize chart 1
				this.titling(max["One"]["district"], 0); // Add title to first
				this.reveal(max["One"]["arr"], 0); // Reveal the first 

				setTimeout( () => {
					this.secondChart.init(statisticTwo); // Initialize chart 1
					this.titling(max["Two"]["district"], 1); // Add title to second
					this.reveal(max["Two"]["arr"], 1); // Reveal the second 
				} ,500);

			}else {
			// If a Category is selected
			// 
				this.primeChart.change(statisticOne); // Change chart 1
				this.titling(max["One"]["district"], 0); // Add title to first
				this.reveal(max["One"]["arr"], 0); // Reveal the first 

				setTimeout( () => {
					this.secondChart.change(statisticTwo); // Change chart 2
					this.titling(max["Two"]["district"], 1); // Add title to second
					this.reveal(max["Two"]["arr"], 1); // Reveal the second 
				} ,400);
			}

			// Toggle Active/inactive class
			el.target.classList.toggle("inactive");
			el.target.classList.toggle("active");

		}else{
			// // If the category is enable, Disable it !
		
			var index = this.activated.indexOf(el.target.getAttribute('data-type'));
			this.activated.splice(index, 1);

			max = this.stat.getMax(this.json, this.activated)

			statisticOne = this.stat.allStats(this.json, max["One"]["arr"])
			statisticTwo = this.stat.allStats(this.json, max["Two"]["arr"])
			nbActive = this.activated.length;

			if(nbActive === 0) {
			// If there is no more category selected, close chart
				
				this.primeChart.terminate(); // Terminate chart 1
				this.titling(null, 3); // Remove title 1
				this.reveal(null, 3); // Unreveal bloc 1
				setTimeout( () => {
					this.secondChart.terminate(); // Terminate chart 1
					this.titling(null, 3); // Remove title 1
				} ,400);
			}else {
			// If a category is already selected, change charts

				this.primeChart.change(statisticOne); // Change chart 1
				this.titling(max["One"]["district"], 0); // Add title to first
				this.reveal(max["One"]["arr"], 0); // Reveal the first 

				setTimeout( () => {
					this.secondChart.change(statisticTwo); // Change chart 2
					this.titling(max["Two"]["district"], 1); // Add title to second
					this.reveal(max["Two"]["arr"], 1); // Reveal the second
				} ,400);
			}

			// Toggle Active/inactive clas
			el.target.classList.toggle("inactive");
			el.target.classList.toggle("active");
		}
	}

	titling (title, container) {
		if (container == 0) {
			$("#prime-district").text(title);
		}
		if (container == 1) {
			$("#second-district").text(title);
		}
		if (container == 3) {
			$("#second-district").text("");
			$("#prime-district").text("");
		}
		
	}

	reveal(item, container) {

		if(container != 3){

			var classe = "arr-" + item
			var element = $('.'+classe);
			var districtClass;

			if(container == 0) {
					districtClass = "district-one";
			}
			if(container == 1) {
				districtClass = "district-two";
			}

			if(! element.hasClass(districtClass) ){

				$('.' + districtClass + ".reveal").removeClass("reveal");
				$('.' + districtClass).removeClass(districtClass);
				
				
				element.addClass(districtClass);
				element.addClass("reveal");	

			}

		}else{
			$(".district-one").removeClass("reveal");
			$(".district-one").removeClass("district-one");
			$(".district-two").removeClass("reveal");
			$(".district-two").removeClass("district-two");
		}
	}

	addClass(researchClass, addedClass) {
		var theID = document.getElementsByClassName(researchClass)[0].attributes.id.nodeValue;
		var element = document.getElementById(theID);
		element.classList.add(addedClass);
	}

	removeClass(researchClass, removedClass) {
		var theID = document.getElementsByClassName(researchClass)[0].attributes.id.nodeValue;
		var element = document.getElementById(theID);
		element.classList.add(addedClass);
	}

	/**
	 * onResize
	 * - Triggered when window is resized
	 * @param  {obj} evt
	 */
	onResize( evt ) {
		this.width = window.innerWidth;
		this.height = window.innerHeight;
	}
}

export default App;
