const ArrayUtils = {

	/**
	 * shuffle
	 * - Shuffle an array
	 * @param  {[Array]} array [Array to shuffle]
	 * @return {[Array]}   Array    [ The shuffled array ]
	 */
	shuffle(array) {
		for (var i = array.length - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
		return array;
	},
	
	/**
	 * getAverage
	 * - Get the average of values in array
	 * @param  {[Array]} arr [Source Array]
	 * @return {[Number]}     [Average of values]
	 */
	getAverage (arr) {
		var count = 0;
		for (let i  = 0 ; i < arr.length; i++) {
			count+= arr[i];
		}
		return count / arr.length;	
	},

	jsonToArray(json) {
		var parsed = JSON.parse(json);

		var arr = [];

		for(var x in parsed){
		  arr.push(parsed[x]);
		}

		return arr;
	},

	sortByValue(arr) {
		arr.sort(function (a, b) {
		    if (a.value > b.value)
		      return 1;
		    if (a.value < b.value)
		      return -1;
		    // a doit être égale à b
		    return 0;
		});

		return arr;
	}

};

export default ArrayUtils;