const ColorUtils = {

	/**
	 * randomFlatColor
	 * - Get an random flat color from flat color array
	 * @return {[String]} [The code of the chosen color]
	 */
	randomFlatColor () {
		const colors = [
			'3498db',
			'2980b9',
			'9b59b6',
			'8e44ad',
			'35cf76',
			'27ae60',
			'e74c3c',
			'e67e22',
			'f39c12'
		];
		return colors[ Math.floor(( Math.random() * (colors.length - 0) + 0))];
	},

	/**
	 * flatBlueSet
	 * - Get an random  blue flat color from blue color array
	 * @return {[String]} [The code of the chosen color]
	 */
	flatBlueSet () {
		const colors = [
			'446CB3',
			'E4F1FE',
			'4183D7',
			'59ABE3',
			'81CFE0',
			'52B3D9',
			'C5EFF7',
			'22A7F0',
			'3498DB',
			'2C3E50',
			'19B5FE',
			'336E7B',
			'22313F'
		];

		return colors[ Math.floor(( Math.random() * (colors.length - 0) + 0))];
	},

	/**
	 * flatRedSet
	 * - Get an random  red flat color from red color array
	 * @return {[String]} [The code of the chosen color]
	 */
	flatRedSet () {
		const colors = [
			'D91E18',
			'F22613',
			'96281B',
			'EF4836',
			'C0392B',
			'CF000F',
			'D64541'
		];

		return colors[ Math.floor(( Math.random() * (colors.length - 0) + 0))];
	},

	/**
	 * flatPurpleSet
	 * - Get an random purple flat color from purple color array
	 * @return {[String]} [The code of the chosen color]
	 */
	flatPurpleSet () {
		const colors = [
			'663399',
			'674172',
			'9B59B6',
			'8E44AD',
			'9A12B3',
			'DCC6E0'
		];

		return colors[ Math.floor(( Math.random() * (colors.length - 0) + 0))];
	},

	/**
	 * flatOrangeSet
	 * - Get an random  orange flat color from orange color array
	 * @return {[String]} [The code of the chosen color]
	 */
	flatOrangeSet () {
		const colors = [
			'F9690E',
			'D35400',
			'F9690E',
			'F5AB35',
			'E9D460',
			'F4B350'
		];

		return colors[ Math.floor(( Math.random() * (colors.length - 0) + 0))];
	},

	/**
	 * flatYellowSet
	 * - Get an random  yellow flat color from yellow color array
	 * @return {[String]} [The code of the chosen color]
	 */
	flatYellowSet () {
		const colors = [
			'F5D76E',
			'F7CA18',
			'F4D03F',
			'E9D460',
			'F9BF3B'
		];

		return colors[ Math.floor(( Math.random() * (colors.length - 0) + 0))];
	}
}

export default ColorUtils;