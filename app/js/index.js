import App from './app';

$( document ).ready(function() {
   
    // Remove all class begin to make a quick animation when the page is opened
    setTimeout(function() {
    	$(".title.begin").removeClass('begin');
    }, 500);

    setTimeout(function() {
    	$(".subtitle.begin").removeClass('begin');
    }, 700);

    setTimeout(function() {
    	 $(".map.begin").removeClass('begin');
    }, 800);

     setTimeout(function() {
    	 $(".notice.begin").removeClass('begin');
    }, 1000);

      setTimeout(function() {
    	 $(".controls.begin").removeClass('begin');
    }, 1000);

    //***********

    // Create an App instance
    const app = new App();
});



